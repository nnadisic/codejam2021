# Code for 1st problem of qualif round of Codejam 2020

using LinearAlgebra

function runme()
    data = readlines(stdin)
    T = parse(Int, popfirst!(data))
    # println(T)
    for idtest in 1:T
        N = parse(Int, popfirst!(data))
        # println(N)
        mat = zeros(Int, N, N)
        for i in 1:N
            for (j, entry) in enumerate(split(popfirst!(data)))
                mat[i,j] = parse(Int, entry)
            end
        end
        # display(mat)
        # println()
        r = 0
        for row in eachrow(mat)
            if length(unique(row)) != length(row)
                r += 1
            end
        end
        c = 0
        for col in eachcol(mat)
            if length(unique(col)) != length(col)
                c += 1
            end
        end
        println("Case #$(idtest): $(tr(mat)) $(r) $(c)")
    end
end

runme()
