# Interactive problem example from Practice Session 2018 - Code Jam 2018

function dostuff()
    T = parse(Int, readline())
    wronganswer = false
    for idtest in 1:T
        if wronganswer
            break
        end
        line1 = readline()
        line2 = readline()
        A, B = [parse(Int, x) for x in split(line1)]
        N = parse(Int, line2)
        # println("$(A) $(B) $(N)")
        guess = 0
        lowerbound = A
        upperbound = B
        for idguess in 1:N
            guess = round(Int, (upperbound + lowerbound) / 2)
            println(guess)
            flush(stdout)
            answer = readline()
            if answer == "WRONG_ANSWER"
                wronganswer = true
                break
            elseif answer == "CORRECT"
                break
            elseif answer == "TOO_SMALL"
                lowerbound = guess
            elseif answer == "TOO_BIG"
                upperbound = guess
            end
        end
    end
end

dostuff()
