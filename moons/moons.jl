
function compcost(S, X, Y)
    nbCJ = 0
    nbJC = 0
    for i in 2:length(S)
        bigram = S[i-1] * S[i]
        if bigram == "CJ"
            nbCJ += 1
        elseif bigram == "JC"
            nbJC += 1
        end
    end
    return nbCJ * X + nbJC * Y
end

function moons()
    data = readlines(stdin)
    T = parse(Int, popfirst!(data))
    for idtest in 1:T
        splitline = split(popfirst!(data))
        X, Y = [parse(Int, x) for x in splitline[1:2]]
        S = split(splitline[3], "")
        pool = [S]
        while true
            curS = popfirst!(pool)
            nextmark = findfirst(isequal("?"), curS)
            if nextmark == nothing
                push!(pool, curS)
                break
            else
                tryC = curS
                tryC[nextmark] = "C"
                costC = compcost(tryC[1:nextmark], X, Y)
                tryJ = copy(tryC)
                tryJ[nextmark] = "J"
                costJ = compcost(tryJ[1:nextmark], X, Y)
                if costC < costJ
                    push!(pool, tryC)
                elseif costC > costJ
                    push!(pool, tryJ)
                else
                    push!(pool, tryC, tryJ)
                end
            end
        end
        allcosts = [compcost(curS, X, Y) for curS in pool]
        cost = minimum(allcosts)
        println("Case #$(idtest): $(cost)")
    end
end

moons()
