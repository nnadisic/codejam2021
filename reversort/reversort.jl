
function reversort()
    data = readlines(stdin)
    T = parse(Int, popfirst!(data))
    # println(T)
    for idtest in 1:T
        N = parse(Int, popfirst!(data))
        elements = [parse(Int, x) for x in split(popfirst!(data))]
        score = 0
        for i in 1:N-1
            j = findfirst(isequal(minimum(elements[i:end])), elements)
            elements[i:j] .= reverse(elements[i:j])
            score += j - i + 1
        end
        # display(elements)
        # println()
        println("Case #$(idtest): $(score)")
    end
end

reversort()
